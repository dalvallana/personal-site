<?php
$subNav = array(
	"Research ; research.php ; #4d90fe;",
	"Publications ; publications.php ; #4d90fe;",
	"Contact me ; contactme.php ; #4d90fe;",
);

set_include_path("../");
include("inc/essentials.php");
?>
<script>
$mainNav.set("about me") // this line colors the top button main nav with the text "home"
</script>
<h1 class="margin-t-0">Publications</h1>
<hr class="light"/>

<h2>2014</h2>
<ul>
	<li>Alvarado, D., & Díaz, P. (2014, May). <strong>Design and evaluation of a platform to support co-design with children</strong>. In <em>Proceedings of the 2014 International Working Conference on Advanced Visual Interfaces</em> (pp. 335-336). ACM.</li>
</ul>

<h2>2013</h2>
<ul>
	<li>Alvarado, D., Díaz, P., & Paredes, P. (2013, September). <strong>ChiCo: a platform to support children co-design</strong>. In <em>Proceedings of the 27th International BCS Human Computer Interaction Conference</em>. BCS L&D Ltd.</li>
</ul>

<h2>2012</h2>
<ul>
	<li>Alvarado, D. (2012, June). <strong>Supporting non-formal learning through co-design of social games with children</strong>. In <em>Proceedings of the 11th International Conference on Interaction Design and Children</em> (pp. 347-350). ACM.</li>
	<li>Giaccardi, E., Paredes, P., Díaz, P., & Alvarado, D. (2012, June). <strong>Embodied narratives: a performative co-design technique</strong>. In <em>Proceedings of the Designing Interactive Systems Conference</em> (pp. 1-10). ACM.</li>
	<li>Diaz, P., Paredes, P., Alvarado, D., & Giaccardi, E. (2012, July). <strong>Co-designing social games with children to support non formal learning</strong>. In <em>Advanced Learning Technologies (ICALT), 2012 IEEE 12th International Conference on</em> (pp. 682-683). IEEE.</li>
</ul>


