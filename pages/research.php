<?php
$subNav = array(
	"Research ; research.php ; #4d90fe;",
	"Publications ; publications.php ; #4d90fe;",
	"Contact me ; contactme.php ; #4d90fe;",
);

set_include_path("../");
include("inc/essentials.php");
?>
<script>
$mainNav.set("about me") // this line colors the top button main nav with the text "home"
</script>
<h1 class="margin-t-0">Research</h1>
<hr class="light"/>

Roughly speaking, my work is oriented to find innovative ways by which children may interact with
novel systems to allow them to become part of an adult design team. In this sense,
my main subjects of investigation are co-design and interaction design for children.<br /><br />

<img src="img/research.jpg" width="300px" style="margin:0 10px 3px 0; float:left">Within the context of a project on emergency preparedness being currently executed in my laboratory,
there is room to work specifically on children’s instruction on emergencies prevention and response.
Through my research, I plan to help children learn how to prevent an emergency and respond to it, if occurred.
My approach is to engage children in the design of an educational game by deploying a <strong>co-design technique</strong>.
The game should be targeted to other children and should aim at instructing them on emergency response and preparation.
Following Vygotsky’s conception of learning as a result of social interactions,
the kind of game that could fit on this purpose at best is a <strong>social game</strong>.
Existent co-design techniques do not focus on social interactions, but on encouraging
motivation and fostering imagination, two features that are equally needed. The technique I want to deploy
should encourage children's creativity in the early stages of the design process,
while allowing them to explore domain-specific aspects of <strong>embodied interaction</strong>.
Such aspects concern preparation and response to emergencies occurring in children’s everyday contexts,
e.g. at home or school. In order to gather all these requirements, I plan to exploit the many possibilities
that current technologies offer. My idea is to develop a digital and social platform by which children
can explore the context and collect ideas collaboratively, and place them on a digital storyboard.
Their creations should serve to adult designers to create engaging, fun and educating games for children.

<blockquote>
	Human learning presupposes a specific social nature and a process by which children
	grow into the intellectual life of those around them”
</blockquote>
<small style="float: right">Vygotsky (1978, p.88).</small><br />
<br />
<br />
<br />
<style>
.toTopArrow{
	opacity:0.5;
	filter: alpha(opacity = 50);
}
.toTopArrow:hover{
	opacity:1;
	cursor:pointer;
	filter: alpha(opacity = 100);	
}
</style>
<div style="width:100%;text-align:center;"><img title="To Top" class="toTopArrow" src="img/arrows/dark/arrowToTop.png" onClick="javascript:$('html,body').animate({scrollTop:0},300);" /></div>