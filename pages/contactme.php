<?php
$subNav = array(
	"Research ; research.php ; #4d90fe;",
	"Publications ; publications.php ; #4d90fe;",
	"Contact me ; contactme.php ; #4d90fe;",
);

set_include_path("../");
include("inc/essentials.php");
?>
<script>
$mainNav.set("more") // this line colors the top button main nav with the text "home"
</script>
<?php

if ( ! empty($_POST['contact']))
{
	$valid = array
	(
		'email'   => array('/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD', 'Please, provide a valid e-mail.'),
		'message' => array('/(.+){10,}/', 'The message must be longer than 10 characters.'),
	);
	
	$errors = array();
	
	foreach ($valid as $field => $data)
	{
		$regex = $data[0];
		$message = $data[1];
		
		$input = trim($_POST[$field]);
		
		if (empty($input) OR ! preg_match($regex, $input))
		{
			$errors += array($field => $message);
		}
	}
	
	$result = empty($errors) ? 'success' : 'errors';
	
	echo json_encode(array
	(
		'result' => $result,
		'errors' => $errors,
	));
	
	if(empty($errors)) {
		mail("daao87@gmail.com","Metro UI template", $_POST['message']."\n\n Sent from: ".$_POST['email'], "From: " .$_POST['email']);
	}
	
	exit;
}

?>

<h1 class="margin-t-0">Contact me</h1>
<hr class="light"/>
<br />
	<div id="container">
		<span class="biggerText">At the moment, the form is disabled. Sorry for that!<br />
		Anyway, if you want to contact me, just write me an e-mail to <strong> diego (dot) alvarado (at) uc3m (dot) es</strong>.<br /><br /></span>
		
		<div id="form">
			<form id="contact" method="post" action="">
				
				
				<input type="text" style="width:400px;" id="email" name="email" class="m-wrap" placeholder="Your e-mail" disabled />
				<br />
				<textarea style="width:400px; height:150px;" id="message" name="message" class="m-wrap" placeholder="Message" disabled></textarea>
				<br />
				<input type="hidden" name="contact" value="1" />
				<button type="submit" class="m-btn blue disabled" disabled>Submit!</button>
				
			</form>
		</div>
	
	</div>	


	<script type="text/javascript">
		
		/**
		 * Just a simple function to enable / disable our submit button
		 * It lets the user know we're working on the request, and something is actually happening.
		 */
		/*(function() {
			$.fn.toggleButton = function() {
				var $this = $(this),
					disabled = $this.attr('disabled');
					
				( ! disabled) ? $this.html('Submitting...').attr('disabled', 'disabled')
							  : $this.html('Send!').removeAttr('disabled');
					
				return $this;
			}
		})();*/
		
		// Shortcut to $(document).ready()
		$(function() {
			
			// placeholder for IE compatibility
			function add() {
	            if($(this).val() === ''){
	                $(this).val($(this).attr('placeholder')).css("color","#999");;
	            }
	        }
	
	        function remove() {
	            if($(this).val() === $(this).attr('placeholder')){
	                $(this).val('').css("color","");;
	            }
	        }
	
	        // Create a dummy element for feature detection
	        if (!('placeholder' in $('<input>')[0])) {
	
	            // Select the elements that have a placeholder attribute
	            $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

	        }
			
			// Attach function to the 'submit' event of the form
			$('#contact').submit(function() {
				
				// Remove the placeholder text before the form is submitted
				$(this).find('input[placeholder], textarea[placeholder]').each(remove);
				
				var self = $(this), 		 // Caches the $(this) object for speed improvements
					post = self.serialize(); // Amazing function that gathers all the form fields data
											 // and makes it usable for the PHP
				
				// Disable the submit button
				self.find('button').toggleButton();
				
				// Send our Ajax Request with the serialized form data
				$.post('pages/contactme.php', post, function(data) {
					// Since we returned a Json encoded string, we need to eval it to work correctly
					var data = eval('(' + data + ')');
					
					// If everything validated and went ok
					if (data.result == 'success') {
						// Fade out the form and add success message
						$('#contact').fadeOut(function() {
							$(this).remove();
							$('<div class="message success"><h4>Thanks for your e-mail!</h4></div>')
								.hide()
								.appendTo($('#form'))
								.fadeIn();
						});
					}
					else {
						// Hide any errors from previous submits
						$('span.error').remove();
						$(':input.error').removeClass('error');
						
						// Re-enable the submit button
						$('#contact').find('button').toggleButton();
						
						// Loop through the errors, and add class and message to each field
						$.each(data.errors, function(field, message) {
							$('#' + field).addClass('error').after('<span class="error">' + message + '</span>');
						});
					}
				});
				
				// Don't let the form re-load the page as would normally happen
				return false;
			});
			
		});
	</script>
