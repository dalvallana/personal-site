<?php
$tileTypes['simpleDiv'] = array( /* Defaults*/
	"group"=>0,
	"x"=>0,
	"y"=>0,
	'width'=>2,
	'height'=>1,
	"background"=>$defaultBackgroundColor,
	"url"=>"",
	"title"=>"The title",
	"text"=>"the description text",
	"img"=>"",
	"imgSize"=>"50",
	"imgToTop"=>"5",
	"imgToLeft"=>"5",
	"labelText"=>"",
	"labelColor"=>$defaultLabelColor,
	"labelPosition"=>$defaultLabelPosition,
	"classes"=>"",
);
function tile_simpleDiv($group,$x,$y,$width,$height,$background,$url,$title,$text,$img,$imgSize,$imgToTop,$imgToLeft,$labelText,$labelColor,$labelPosition,$classes){
	global $scale, $spacing, $scaleSpacing, $groupSpacing;
    
	$marginTop = $y*$scaleSpacing+getMarginTop($group);
	$marginLeft = $x*$scaleSpacing+getMarginLeft($group);
	$tileWidth = $width*$scaleSpacing-$spacing;
	$tileHeight = $height*$scaleSpacing-$spacing;
	if($img == ""){
		$hasImg = 0;
	}else{
		$hasImg = 1;
	}
	?>
  	<div class="tile group<?php echo $group?> <?php echo $classes?>" style="
    margin-top:<?php echo $marginTop;?>px; margin-left:<?php echo $marginLeft;?>px;
	width:<?php echo $tileWidth?>px; height:<?php echo $tileHeight?>px;
	background:<?php echo $background;?>;" <?php posVal($marginTop,$marginLeft,$tileWidth);?>> 
    <?php if($img != ""){?>
    <img style='float:left; margin-top:<?php echo $imgToTop?>px;margin-left:<?php echo $imgToLeft?>px;' 
    src='<?php echo $img?>' height="<?php echo $imgSize?>" width="<?php echo $imgSize?>"/>
    <?php } ?>
	<div class='tileTitle' style='margin-left:<?php echo ($imgSize+$imgToLeft)*$hasImg+10;?>px;'><?php echo $title?></div>
	<div class='tileDesc' style='margin-left:<?php echo ($imgSize+$imgToLeft)*$hasImg+12;?>px;'><?php echo $text?></div>
    <?php
    if($labelText!=""){
		if($labelPosition=='top'){
			echo "<div class='tileLabelWrapper top' style='border-top-color:".$labelColor.";'><div class='tileLabel top' >".$labelText."</div></div>";
		}else{
			echo "<div class='tileLabelWrapper bottom'><div class='tileLabel bottom' style='border-bottom-color:".$labelColor.";'>".$labelText."</div></div>";
		}
	}?> 
    </div>
    <?php
}
?>