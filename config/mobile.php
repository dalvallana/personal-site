<?php
/*MOBILE CONFIG*/

$enableMobile = true; // enable mobile version of template
$redirectPhone = true; // redirect phones
$redirectTablet = false; //redirect tablets

$groupSpacing_mob = array(5.5,5.5);

$mobileZoomable = false; // enable pinch-to-zoom or not?
$showFullSiteLink = true; // show link to full site in footer of mobile site

?>