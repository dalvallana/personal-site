<?php
/* All tiles on the homepage are configured here, be sure to check the tutorials/docs on http://metro-webdesign.info */

/* GROUP 1 */

$tile[] = array("type"=>"simple","group"=>0,"x"=>0,"y"=>0,'width'=>3,'height'=>1,"background"=>"#3B659C","url"=>"",
"title"=>"Hello World!","text"=>"I'm a Research Assistant and a PhD. Student at the Department of Computer Science of Universidad Carlos III de Madrid.",
"img"=>"img/diego.jpg","imgSize"=>"145","imgToTop"=>"0","imgToLeft"=>"0","labelText"=>"","labelColor"=>"","labelPosition"=>"","classes"=>"noClick");

$tile[] = array("type"=>"scrollText","group"=>0,"x"=>3,"y"=>0,"width"=>1,"height"=>1,"background"=>"#FFCC00","url"=>"",
"title"=>"Interests","text"=>array(
"HCI<br>Usability<br>User Experience",
"Software Engineering<br>Mobile computing<br>Web development",
"Networking<br>Traveling<br>Tennis<br>Football (Soccer)"
),"scrollSpeed"=>2500,"classes"=>"noClick blackTextTile");

$tile[] = array("type"=>"simple","group"=>0,"x"=>0,"y"=>1,'width'=>2,'height'=>1,"background"=>"#854C8F","url"=>"",
"title"=>"","text"=>"Department of Computer Science.<br>Universidad Carlos III de Madrid<br>
Avda. de la Universidad, 30<br>28911 Legan�s, Madrid - Spain<br>Office: 2.2.C.02B<br>
Phone: +34 916246260<br>Email: <img src='img/email.gif'/>",
"classes"=>"noClick");

$tile[] = array("type"=>"simple","group"=>0,"x"=>2,"y"=>1,'width'=>1,'height'=>1,"background"=>"#A7C8FC","url"=>"research.php",
"title"=>"<span class='smallerTileTitle blackTextTile'>Research</span>","text"=>"","classes"=>"");

$tile[] = array("type"=>"simple","group"=>0,"x"=>3,"y"=>1,'width'=>1,'height'=>1,"background"=>"#C33","url"=>"publications.php",
"title"=>"<span class='smallerTileTitle'>Publications</span>","text"=>"","classes"=>"");

$tile[] = array("type"=>"simpleDiv","group"=>0,"x"=>0,"y"=>2,'width'=>1,'height'=>1,"background"=>"#F90","url"=>"",
"title"=>"Resume",
"text"=>"- <a class='linkInTile' href='files/cv_english.pdf' target='_blank'>English</a><br />
- <a class='linkInTile' href='files/cv_espanol.pdf' target='_blank'>Espa�ol</a><br />
- <a class='linkInTile' href='files/cv_deutsch.pdf' target='_blank'>Deutsch</a><br />
- <a class='linkInTile' href='myresume' target='_blank'>Online (informal)</a>",
"classes"=>"noClick");

$tile[] = array("type"=>"simpleDiv","group"=>0,"x"=>1,"y"=>2,'width'=>2,'height'=>1,"background"=>"#789600","url"=>"",
"title"=>"External links",
"text"=>"- <a class='linkInTile' href='http://www.uc3m.es' target='_blank'>Universidad Carlos III de Madrid</a><br />
- <a class='linkInTile' href='http://dei.inf.uc3m.es' target='_blank'>DEI Lab</a>",
"img"=>"img/icons/world.gif","imgSize"=>"50","imgToTop"=>"2","imgToLeft"=>"5",
"labelText"=>"Go to official sites","labelColor"=>"#3B659C","labelPosition"=>"bottom","classes"=>"noClick");

$tile[] = array("type"=>"slide","group"=>0,"x"=>3,"y"=>2,'width'=>1,'height'=>1,"background"=>"#333","url"=>"",
	"text"=>"<h5>Go to last tilegroup</h5>","img"=>"img/arrows/light/arrowRight.png","imgSize"=>0.25,
	"slidePercent"=>0.50,
	"slideDir"=>"up", // can be up, down, left or right
	"doSlideText"=>true,"doSlideLabel"=>true,"classes"=>"moveTo tileGroup1"
);

/*$tile[] = array("type"=>"slideshow","group"=>0,"x"=>0,"y"=>1,"width"=>1,"height"=>1,"background"=>"#6950ab","url"=>"",
	"images"=>array("img/img1.png","img/img2.jpg","img/img3.jpg"),
	"effect"=>"slide-right","speed"=>5000,"arrows"=>true,
	"labelText"=>"Slideshow","labelColor"=>"#11528f","labelPosition"=>"bottom",
	"classes"=>"noClick");

$tile[] = array("type"=>"scrollText","group"=>0,"x"=>1,"y"=>1,"width"=>2,"height"=>1,"background"=>"#FF8000","url"=>"external:panels/example.php",
"title"=>"Click to open a sidepanel","text"=>array(
"A sidepanel will come from the right, watch out!",
"Okay, and what you are watching now is a scroll live tile...",
"which can be very cool",
"to open a sidepanel, check this source code in tiles.php"
),"scrollSpeed"=>2500);

$tile[] = array("type"=>"simple","group"=>0,"x"=>0,"y"=>2,'width'=>2,'height'=>1,"background"=>"#6950AB","url"=>"newtab:http://metro-webdesign.info/#!/docs-and-tutorials",
	"title"=>"<span style='font-size:24px;'>Check the tutorials</span>",
	"text"=>"be <em>CREATIVE</em> and <em>MODIFY</em> this example. It's just an example, play with the tiles!",
	"img"=>"img/icons/box_warning.png","imgSize"=>"50","imgToTop"=>"5","imgToLeft"=>"5",
	"labelText"=>"Metro-Webdesign","labelColor"=>"#453B5E","labelPosition"=>"bottom");

$tile[] = array("type"=>"custom","group"=>0,"x"=>3,"y"=>0,'width'=>1,'height'=>1,"background"=>"#11528f","url"=>"typography.php",
"content"=>
"<div style='line-height:30px; margin-top:5px;'>
<div style='color:#FFF;font-size:43px;line-heigt:70px;'><strong>CHECK</strong></div>
<span style='color:#FFF;font-size:32px;'><strong>OUT</strong></span><span style='color:#BBB;font-size:32px;'>THE</span>
<div style='font-size:57px;line-height:30px;'>TYPO</div>
<div style='font-size:37px;line-height:40px;'>GRAPHY</div>
</div>");*/

/* GROUP 2*/

$tile[] = array("type"=>"flip","group"=>1,"x"=>0,"y"=>0,'width'=>1,'height'=>1,"background"=>"#cc3333","url"=>"contactme.php",
	"direction"=>"horizontal","text"=>"<h5>E-mail me!</h5>","img"=>"img/icons/gmail.gif","imgSize"=>1,
	"slidePercent"=>0.50,
	"slideDir"=>"up", // can be up, down, left or right
	"doSlideText"=>true,"doSlideLabel"=>true,"classes"=>"");

$tile[] = array("type"=>"simple","group"=>1,"x"=>1,"y"=>0,'width'=>1,'height'=>1,"background"=>"#0099cc",
"url"=>"newtab:http://www.linkedin.com/pub/diego-alvarado-orellana/18/242/1a3","title"=>"","text"=>"",
"img"=>"img/icons/linkedin.gif","imgSize"=>"145","imgToTop"=>"0","imgToLeft"=>"0","labelText"=>"","labelColor"=>"","labelPosition"=>"","classes"=>"");

$tile[] = array("type"=>"img","group"=>1,"x"=>2,"y"=>0,'width'=>2,'height'=>1,"background"=>"#333",
"url"=>"newtab:http://uc3m.academia.edu/DiegoAlvarado","title"=>"","text"=>"",
"img"=>"img/icons/academia.jpg","desc"=>"...and maybe follow me.","showDescAlways"=>false,"imgWidth"=>2,"imgHeight"=>1,
"labelText"=>"Visit my profile","labelColor"=>"#854C8F","labelPosition"=>"bottom","classes"=>"");

$tile[] = array("type"=>"simple","group"=>1,"x"=>0,"y"=>1,'width'=>1,'height'=>1,"background"=>"#555",
"url"=>"newtab:http://plus.google.com/u/0/108938157614370136095","title"=>"","text"=>"",
"img"=>"img/icons/gplus.gif","imgSize"=>"145","imgToTop"=>"0","imgToLeft"=>"0","labelText"=>"","labelColor"=>"","labelPosition"=>"","classes"=>"");

$tile[] = array("type"=>"img","group"=>1,"x"=>1,"y"=>1,'width'=>1,'height'=>1,"background"=>"#7B0017",
"url"=>"newtab:http://www.mendeley.com/profiles/diego-alvarado-orellana/","title"=>"","text"=>"",
"img"=>"img/icons/mendeley.gif","desc"=>"Connect with my work","showDescAlways"=>false,"imgWidth"=>1,"imgHeight"=>1,
"labelText"=>"Mendeley","labelColor"=>"#666","labelPosition"=>"top","classes"=>"");

$tile[] = array("type"=>"simple","group"=>1,"x"=>2,"y"=>1,'width'=>1,'height'=>1,"background"=>"#336699",
"url"=>"newtab:http://facebook.com/daao87","title"=>"","text"=>"",
"img"=>"img/icons/facebook.gif","imgSize"=>"145","imgToTop"=>"0","imgToLeft"=>"0","labelText"=>"","labelColor"=>"","labelPosition"=>"","classes"=>"");

$tile[] = array("type"=>"simple","group"=>1,"x"=>3,"y"=>1,'width'=>1,'height'=>1,"background"=>"#99ccff",
"url"=>"newtab:https://twitter.com/daao87","title"=>"","text"=>"",
"img"=>"img/icons/twitter.gif","imgSize"=>"145","imgToTop"=>"0","imgToLeft"=>"0","labelText"=>"","labelColor"=>"","labelPosition"=>"","classes"=>"");

$tile[] = array("type"=>"flip","group"=>1,"x"=>1,"y"=>2,'width'=>1,'height'=>1,"background"=>"#00ccff","url"=>"",
	"direction"=>"vertical","text"=>"<h5>dieguistieri</h5>","img"=>"img/icons/skype.gif","imgSize"=>1,
	"classes"=>"noClick");

$tile[] = array("type"=>"simpleDiv","group"=>1,"x"=>2,"y"=>2,'width'=>2,'height'=>1,"background"=>"#FFCC00","url"=>"",
"title"=>"","text"=>"<br>In case you didn't notice, the design of this page was based on
<a class='linkInTile' href='http://en.wikipedia.org/wiki/Metro_(design_language)' target='_blank'>Metro UI</a>. =)",
"img"=>"img/icons/windows8.gif","imgSize"=>"145","imgToTop"=>"0","imgToLeft"=>"0",
"labelText"=>"","labelColor"=>"","labelPosition"=>"","classes"=>"noClick");


/*$tile[] = array("type"=>"slidefx","group"=>1,"x"=>0,"y"=>0,'width'=>2,'height'=>1,"background"=>"#333","url"=>"external:img/metro_slide.png",
	"text"=>"Click to see in full","img"=>"img/metro_slide_300x150.png","classes"=>"lightbox"
);

$tile[] = array("type"=>"slide","group"=>1,"x"=>0,"y"=>1,'width'=>2,'height'=>1,"background"=>"#00BFFF","url"=>"sidebars.php",
	"text"=>"<h3>A page with sidebar</h3>","img"=>"img/metro_slide_300x150_2.png","imgSize"=>1,
	"slidePercent"=>0.40,
	"slideDir"=>"up", // can be up, down, left or right
	"doSlideText"=>true,"doSlideLabel"=>true,
	"labelText"=>"A slide tile","labelColor"=>"#00BFFF","labelPosition"=>"top",
);

$tile[] = array("type"=>"slideshow","group"=>1,"x"=>2,"y"=>0,"width"=>1,"height"=>1,"background"=>"#6950ab","url"=>"newtab:http://google.com",
	"images"=>array("img/chars/a.png","img/chars/b.png","img/chars/c.png","img/chars/d.png","img/chars/e.png","img/chars/f.png","img/chars/g.png"),
	"effect"=>"slide-right, slide-left, slide-down, slide-up, flip-vertical, flip-horizontal, fade",
	"speed"=>1500,"arrows"=>false,
	"labelText"=>"Random fx","labelColor"=>"#453B5E","labelPosition"=>"top");
	
$tile[] = array("type"=>"flip","group"=>1,"x"=>2,"y"=>1,'width'=>1,'height'=>1,"background"=>"#C82345","url"=>"accordions.php","img"=>"img/metro_150x150.png",
	"text"=>"<h4 style='color:#FFF;'>Click for accordions!</h4>");
	*/
	
/* GROUP 3 */
/*
$tile[] = array("type"=>"img","group"=>2,"x"=>0,"y"=>0,'width'=>1,'height'=>1,"background"=>"#0F6D32","url"=>"",
	"img"=>"img/img3.jpg","desc"=>"By adding the CSS class 'noClock' to this tile, we've achieved that there is no hover effect!",
	"showDescAlways"=>true,"imgWidth"=>1,"imgHeight"=>1,
	"labelText"=>"Img Tile","labelColor"=>"#509601","labelPosition"=>"bottom", "classes"=>"noClick");
	
$tile[] = array("type"=>"slide","group"=>2,"x"=>1,"y"=>0,'width'=>2,'height'=>1,"background"=>"#FE2E64","url"=>"",
	"text"=>"<h3>No click</h3>","img"=>"img/metro_slide_300x150_2.png","imgSize"=>1,
	"slidePercent"=>0.50,
	"slideDir"=>"left", // can be up, down, left or right
	"doSlideText"=>false,"doSlideLabel"=>false,
	"labelText"=>"Other direction slide","labelColor"=>"#CC1A46","labelPosition"=>"top",
	"classes"=>"noClick"
);*/

?> 